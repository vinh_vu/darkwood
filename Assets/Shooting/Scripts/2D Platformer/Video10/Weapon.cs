﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public float damageDelay = 0.8f;
    public float fireRate = 0;
    public float Damage = 10;
    public float dispersion = 10f;
    public LayerMask whatToHit;

    public ParticleSystem bloodSplatterEffect;
    public GameObject BulletTrailPrefab;
    public GameObject MuzzleFlashPrefab;
    float timeToSpawnEffect = 0;
    public float effectSpawnRate = 10;

    float timeToFire = 0;
    Transform firePoint;
    Transform firePoint1;
    Transform firePoint2;
    // Use this for initialization
    void Awake()
    {
        firePoint = transform.FindChild("FirePoint");
        firePoint1 = transform.FindChild("FirePoint1");
        firePoint2 = transform.FindChild("FirePoint2");
        if (firePoint == null)
        {
            Debug.LogError("No firePoint? WHAT?!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (fireRate == 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
        else
        {
            if (Input.GetButton("Fire1") && Time.time > timeToFire)
            {
                timeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }
    }

    Vector2 fire1 = new Vector2();
    Vector2 fire2 = new Vector2();
    Vector2 direction;
    void Shoot()
    {
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

        float random = Random.Range(0, 1f);
        fire1.Set(firePoint1.position.x, firePoint1.position.y);
        fire2.Set(firePoint2.position.x, firePoint2.position.y);
        Vector2 firePointPosition = Vector2.Lerp(fire1, fire2, random);
        //Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 100, whatToHit);
      
        direction = (mousePosition - firePointPosition);
        Effect(direction);
        Debug.DrawLine(firePointPosition, direction * 100, Color.cyan);
        if (hit.collider != null)
        {
            //float delay = (Vector2.Distance(firePointPosition, hit.point))/20f;
           StartCoroutine( DamageObject(damageDelay, firePointPosition, hit, direction));
        }
    }

    private IEnumerator DamageObject(float delay,Vector2 firePointPosition, RaycastHit2D hit, Vector2 direction)
    {
        yield return new WaitForSeconds(delay);
        Debug.DrawLine(firePointPosition, hit.point, Color.red);
        hit.collider.SendMessage("Damage", Damage);
        bloodSplatterEffect.transform.rotation = Quaternion.Euler(180 + Mathf.Tan(direction.y / direction.x), 90, -90);
        bloodSplatterEffect.transform.position = hit.point;
        bloodSplatterEffect.Play();
    }

    void Effect(Vector2 direction)
    {
        if (BulletTrailPrefab)
        {
           GameObject trailer = Instantiate(BulletTrailPrefab, firePoint.position, firePoint.rotation); 
            trailer.transform.forward = direction.normalized;
            //Destroy(trailer.gameObject, damageDelay);
        }
        if (MuzzleFlashPrefab)
        {
            GameObject clone = Instantiate(MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as GameObject;
            clone.transform.parent = firePoint;
            float size = Random.Range(0.1f, 0.2f);
            clone.transform.localScale = new Vector3(size, size, size);
            Destroy(clone, 0.02f);   
        }
    }
}
