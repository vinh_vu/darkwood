﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEditor;

namespace Completed
{
    public class Loader : MonoBehaviour
    {
        public GameObject gameManager;          //GameManager prefab to instantiate.
        public GameObject soundManager;         //SoundManager prefab to instantiate.
        public UnityEvent OnStarted;

        void Awake()
        {
            if (OnStarted != null)
                OnStarted.Invoke();
            //Check if a GameManager has already been assigned to static variable GameManager.instance or if it's still null
            if (GameManager.instance == null)

                //Instantiate gameManager prefab
                Instantiate(gameManager);

            //Check if a SoundManager has already been assigned to static variable GameManager.instance or if it's still null
            if (SoundManager.instance == null)

                //Instantiate SoundManager prefab
                Instantiate(soundManager);
        }

        private void Load()
        {
        
        }

        //[MenuItem("MyTools/CreateGameObjects")]
        //static void Create()
        //{
        //    GameObject go = new GameObject("Loader");
        //    go.AddComponent<Loader>();
        //}
    }
}