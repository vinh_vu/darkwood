﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody2D rigidBody;
    public int speed = 5;

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody2D>();

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A))
        {
            rigidBody.AddForce(new Vector2(-speed, 0), ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rigidBody.AddForce(new Vector2(speed, 0), ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.W))
        {
            rigidBody.AddForce(new Vector2(0,speed), ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.S))
        {
            rigidBody.AddForce(new Vector2(0, -speed), ForceMode2D.Impulse);
        }
    }
}
