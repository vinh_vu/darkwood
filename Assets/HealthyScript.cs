﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthyScript : MonoBehaviour
{

    public float hp = 100;
    public GameObject bloodDecal;

    void Damage(float damage)
    {
        if (hp > 0)
        {
            hp -= damage;
            if (hp <= 0)
            {
                bloodDecal.SetActive(true);
            }
        }
    }
}
