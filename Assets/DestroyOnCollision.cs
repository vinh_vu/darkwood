﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("bullet Collision");
        Destroy(transform.gameObject);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        print("bullet Collision");
        Destroy(transform.gameObject);
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        print("bullet Collision");
        Destroy(transform.gameObject);
    }
}
