﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour {

    public string DamageMethodName = "Damage";
    public float damage = 30f;
    public float speed = 100f;
    public LayerMask whatToHit;
    // Update is called once per frame
    void Update () {
        transform.position += transform.forward.normalized * Time.deltaTime * speed;
       // transform.position +=   Vector3.right * speed;
    }
     
}
